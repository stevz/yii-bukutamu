<?php

namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\Bukutamu;

class BukutamuController extends Controller
{
    public function actionIndex()
    {
        $query = Bukutamu::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $data = $query->orderBy('email')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'data' => $data,
            'pagination' => $pagination,
        ]);
    }
}